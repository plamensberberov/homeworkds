#include "LZWCompressor.h"

#include <fstream>
#include <ranges>
#include <sstream>

std::string LZWCompressor::readFile(std::filesystem::path path)
{
    std::ifstream file(path, std::ios::in);
    std::string fContent(std::istreambuf_iterator<char>(file), {});

    return fContent;
}

void LZWCompressor::initCompressionTable(HashTable<std::string, unsigned short>& dict)
{
    for (unsigned short ch = 0; ch < 256; ch++)
    {
        dict.insert({ std::string(1,ch), {ch} });
    }
}

void LZWCompressor::initDecompressionTable(HashTable<unsigned short, std::string>& dict)
{
    for (unsigned short ch = 0; ch < 256; ch++)
    {
        dict.insert({ {ch}, std::string(1,ch) });
    }
}
std::string LZWCompressor::compress(std::filesystem::path path)
{
    std::string input = readFile(path);
    std::stringstream outStr;
    HashTable<std::string, unsigned short> dictionary(4096);
    initCompressionTable(dictionary);
    unsigned short code = 256;
    std::string p = std::string(1, input[0]);

    for (const auto& curr : input | std::views::drop(1))
    {
        std::string seq = p + curr;
        if (dictionary.contains(seq))
        {
            p.push_back(curr);
        }
        else
        {
            auto output = dictionary.find(p);
            outStr.write(reinterpret_cast<char*>(&(output->value)), sizeof(unsigned short));
            if (code < 4096)
            {
                dictionary.insert({ seq, code });
                code++;
            }
            p = curr;
        }
    }
    auto output = dictionary.find(p);
    outStr.write(reinterpret_cast<char*>(&(output->value)), sizeof(unsigned short));

    return std::string(outStr.str());
}

std::string LZWCompressor::compressDirectory(std::filesystem::path input)
{
    std::string result;
    std::filesystem::directory_iterator fileList(input);

    for (auto entry : fileList)
    {
        if (entry.is_regular_file())
        {
            result += compress(entry.path());
        }
        else if (entry.is_directory())
        {
            result += compressDirectory(entry.path());
        }
    }

    return result;
}

void LZWCompressor::decompress(std::string content, std::filesystem::path fullPath)
{
    std::ofstream decompressedFile(fullPath);
    if (decompressedFile.good())
    {
    }
    std::stringstream outFile(content), decompressed{};
    HashTable<unsigned short, std::string> decompressionDict(10000);
    unsigned short curr;
    unsigned short code = 256;
    initDecompressionTable(decompressionDict);
    outFile.read(reinterpret_cast<char*>(&curr), sizeof(unsigned short));
    std::string oldTranslation = decompressionDict.find(curr)->value;
    decompressed << oldTranslation;

    while (!outFile.eof())
    {
        outFile.read(reinterpret_cast<char*>(&curr), sizeof(unsigned short));
        std::string res;
        auto found = decompressionDict.find(curr);
        if(found)
        {
            auto translation = found->value;
            res = translation;
        }
        else
        {
            res = oldTranslation + oldTranslation[0];
        }

        decompressionDict.insert({ code++, oldTranslation + res[0] });
        oldTranslation = res;
        decompressed << res;
    }

    decompressedFile << decompressed.str();
}
