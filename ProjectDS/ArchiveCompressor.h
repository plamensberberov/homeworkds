#pragma once
#include <string>
#include <vector>
#include <filesystem>

#include "LZWFile.h"

class ArchiveCompressor
{
public:
	static void compress(std::filesystem::path outPath, std::vector<std::filesystem::path> files);
	static std::string compressDirectory(std::filesystem::path dir, std::filesystem::path base = "");
	static std::string decompress(std::filesystem::path compressed, std::filesystem::path base = "");
	static std::string decompressDirectory(std::filesystem::path compressed);
	static bool verifyChecksum(size_t checksum, std::string content);
};

