#pragma once
#include <string>
#include <filesystem>
#include <functional>

struct LZWFile
{
	std::filesystem::path filename;
	size_t checksum;
	std::string content;
	bool isEmptyDirectory;
	LZWFile(std::filesystem::path filename, bool isEmptyDir = false) : filename(filename), isEmptyDirectory(isEmptyDir)
	{
		compressContent();
		checksum = std::hash<std::string>{}(this->content);
	}
private:
	void compressContent();
};

