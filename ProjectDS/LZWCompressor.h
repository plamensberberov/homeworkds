#pragma once
#define Dict HashTable

#include <string>
#include <filesystem>
#include <unordered_map>

#include "HashTable.h"

class LZWCompressor
{
	static void initCompressionTable(HashTable<std::string, unsigned short>& dict);
	static void initDecompressionTable(HashTable<unsigned short, std::string>& dict);
	static std::string readFile(std::filesystem::path path);
public:
	static std::string compress(std::filesystem::path input);
	static std::string compressDirectory(std::filesystem::path path);
	static void decompress(std::string content, std::filesystem::path decompressed);
};

