#include "ArchiveCompressor.h"
#include <sstream>
#include <fstream>
#include "LZWCompressor.h"

void ArchiveCompressor::compress(std::filesystem::path outPath, std::vector<std::filesystem::path> files)
{
	std::stringstream output;
	for (auto file : files)
	{
		if (std::filesystem::is_regular_file(file))
		{
			LZWFile curr(file);
			curr.filename = curr.filename.filename();

			output.write(reinterpret_cast<char*>(&curr.checksum), sizeof(size_t));
			size_t nameLength = curr.filename.string().length();
			output.write(reinterpret_cast<char*>(&nameLength), sizeof(size_t));
			output.write(const_cast<char*>(curr.filename.string().c_str()), nameLength * sizeof(char));
			size_t contentLength = curr.content.length();
			output.write(reinterpret_cast<char*>(&contentLength), sizeof(size_t));
			output.write(const_cast<char*>(curr.content.c_str()), contentLength * sizeof(char));
		}
		else if (std::filesystem::is_directory(file))
		{
			output << compressDirectory(file, file);
		}
	}

	std::ofstream outFile(outPath, std::ios::binary | std::ios::trunc);

	size_t fullSum = std::hash<std::string>{}(output.str());
	outFile.write(reinterpret_cast<char*>(&fullSum), sizeof(size_t));
	outFile << output.str();
}

std::string ArchiveCompressor::compressDirectory(std::filesystem::path dir, std::filesystem::path base)
{
	std::stringstream result;
	std::filesystem::directory_iterator fileList(dir);
	bool empty = true;

	for (auto entry : fileList)
	{
		empty = false;
		if (entry.is_regular_file())
		{
			LZWFile curr(entry.path());
			curr.filename = std::filesystem::relative(curr.filename, base);

			result.write(reinterpret_cast<char*>(&curr.checksum), sizeof(size_t));
			size_t nameLength = curr.filename.string().length();
			result.write(reinterpret_cast<char*>(&nameLength), sizeof(size_t));
			result.write(const_cast<char*>(curr.filename.string().c_str()), nameLength * sizeof(char));
			result.write(reinterpret_cast<char*>(&empty), sizeof(bool));
			size_t contentLength = curr.content.length();
			result.write(reinterpret_cast<char*>(&contentLength), sizeof(size_t));
			result.write(const_cast<char*>(curr.content.c_str()), contentLength * sizeof(char));
		}
		else if (entry.is_directory())
		{
			result << compressDirectory(entry.path(), base);
		}
	}

	if (empty)
	{
		std::filesystem::path filename = std::filesystem::relative(dir, base);
		size_t checksum = std::hash<std::string>{}("");

		result.write(reinterpret_cast<char*>(&checksum), sizeof(size_t));
		size_t nameLength = filename.string().length();
		result.write(reinterpret_cast<char*>(&nameLength), sizeof(size_t));
		result.write(const_cast<char*>(filename.string().c_str()), nameLength * sizeof(char));
		result.write(reinterpret_cast<char*>(&empty), sizeof(bool));
	}

	return std::string(result.str());
}

std::string ArchiveCompressor::decompress(std::filesystem::path compressed, std::filesystem::path base)
{
	std::ifstream compressedF(compressed, std::ios::binary);
	std::string fContent(std::istreambuf_iterator<char>(compressedF), {});
	std::stringstream compressedFile(fContent);

	size_t fullSum, partialSum;
	compressedFile.read(reinterpret_cast<char*>(&fullSum), sizeof(size_t));
	if (!verifyChecksum(fullSum, fContent.substr(sizeof(size_t))))
	{
		throw std::logic_error("Corrupt file!");
	}

	size_t currLen;

	while (!compressedFile.eof())
	{
		compressedFile.read(reinterpret_cast<char*>(&partialSum), sizeof(size_t));
		compressedFile.read(reinterpret_cast<char*>(&currLen), sizeof(size_t));

		std::string content;
		content.resize(currLen);
		compressedFile.read(const_cast<char*>(content.c_str()), currLen * sizeof(char));
		std::filesystem::path path(content);
		path = std::filesystem::absolute(base / path);
		
		std::filesystem::path parentPath = path.parent_path();
		if(parentPath != "")
			std::filesystem::create_directories(parentPath);
		
		bool emptyDir = false;
		compressedFile.read(reinterpret_cast<char*>(&emptyDir), sizeof(bool));
		if (emptyDir)
		{
			std::filesystem::create_directory(path);
			continue;
		}

		compressedFile.read(reinterpret_cast<char*>(&currLen), sizeof(size_t));
		content.resize(currLen);
		compressedFile.read(const_cast<char*>(content.c_str()), currLen * sizeof(char));

		LZWCompressor::decompress(content, path);
	}

	return std::string();
}

std::string ArchiveCompressor::decompressDirectory(std::filesystem::path compressed)
{
	return std::string();
}

bool ArchiveCompressor::verifyChecksum(size_t checksum, std::string content)
{
	return std::hash<std::string>{}(content) == checksum;
}
