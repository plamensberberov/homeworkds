#pragma once
#include <list>
#include <utility>
#include <vector>

template<typename K, typename V>
class HashTable
{
	struct pair
	{
	public:
		K key;
		V value;
	};
	std::vector<std::list<pair>> _data;
	size_t capacity;
	size_t getIndex(K key);
	std::hash<K> hasher;
public:
	HashTable(size_t capacity = 10000);
	~HashTable();
	HashTable(HashTable& other) = delete;
	HashTable& operator=(HashTable& other) = delete;
	bool contains(K key);
	pair* find(K key)
	{
		size_t index = getIndex(key);

		for (auto& curr : _data[index])
		{
			if (curr.key == key)
			{
				return &curr;
			}
		}

		return nullptr;
	}
	void insert(pair entry);
};

template<typename K, typename V>
inline void HashTable<K, V>::insert(pair entry)
{
	_data[getIndex(entry.key)].push_back(entry);
}

template<typename K, typename V>
size_t HashTable<K, V>::getIndex(K key)
{
	return hasher(key) % capacity;
}

template<typename K, typename V>
inline HashTable<K, V>::HashTable(size_t capacity)
{
	_data = std::vector<std::list<pair>>(capacity, std::list<pair>());
	hasher = std::hash<K>();
	this->capacity = capacity;
}

template<typename K, typename V>
inline HashTable<K, V>::~HashTable()
{
	//delete[] _data;
	//_data = nullptr;
	//capacity = 0;
}

template<typename K, typename V>
inline bool HashTable<K, V>::contains(K key)
{
	size_t index = getIndex(key);
	std::list<pair> bucket = _data[index];

	for (auto& curr : bucket)
	{
		if (curr.key == key)
		{
			return true;
		}
	}

	return false;
}

