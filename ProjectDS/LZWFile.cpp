#include "LZWFile.h"

#include "LZWCompressor.h"

void LZWFile::compressContent()
{
	if (std::filesystem::is_directory(this->filename))
		this->content = LZWCompressor::compressDirectory(this->filename);
	else
		this->content = LZWCompressor::compress(this->filename);
}
