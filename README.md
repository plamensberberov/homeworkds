# .lzw File Format

 - Global checksum - size_t
 - List of ***Blocks***

# *Block* format
## Case 1: Regular file 

 - Partial checksum - size_t
 - Path length - size_t
 - Path - std::string
 - isEmptyDirectory - always false
 - Content length - size_t
 - Content (LZW compressed) - std::string

 ## Case 2: Empty directory 

 - Partial checksum - size_t
 - Path length - size_t
 - Path - std::string
 - isEmptyDirectory - always true
